import java.util.Scanner;

public class ReverseWords {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Write sentence:");
        reverseWords(scanner);
    }

    private static void reverseWords(Scanner scanner) {
        String orginalString = scanner.nextLine();
        String[] arrayOfStrings = orginalString.toLowerCase().split(" ");
        String lastWordFromArray = arrayOfStrings[arrayOfStrings.length - 1];
        char firstLetterBig = Character.toUpperCase(lastWordFromArray.charAt(0));
        String resultLastWord = firstLetterBig + lastWordFromArray.substring(1);
        arrayOfStrings[arrayOfStrings.length - 1] = resultLastWord;

        for (int i = arrayOfStrings.length - 1; i >= 0; i--) {
            System.out.print(arrayOfStrings[i] + " ");
        }
    }
}